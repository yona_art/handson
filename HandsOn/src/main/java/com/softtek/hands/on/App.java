package com.softtek.hands.on;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.softtek.hands.on.DB.DBConection;
import com.softtek.hands.on.Models.Device;

/**
 * Hello world!
 */
public final class App {
    private App() {
    }

    /**
     * Says hello to the world.
     * 
     * @param args The arguments of the program.
     */
    public static void main(String[] args) {
        DBConection con = DBConection.getDbConection();

        List<Device> LDevice = con.getAllDevices();

        List<String> filterList = LDevice.stream().map(Device::getName).filter((name) -> name == "laptops")
                .collect(Collectors.toList());

        System.out.println("The list of devices where the name are laptops");
        filterList.forEach((device) -> device.toString());

        Long countList = LDevice.stream().filter((color) -> color.getManufacturerId() == 3).count();
        System.out.println("The count of devices where manofacturer id is :" + countList);

        List<Device> deviceForColorlist = LDevice.stream().filter((device) -> device.getColorId() == 1)
                .collect(Collectors.toList());

        System.out.println("The Devices with colorid 1 are");
        if (deviceForColorlist.size() > 0) {
            deviceForColorlist.forEach((device) -> device.toString());
        } else {
            System.out.println("Not have devices with color id");
        }

        Map<Integer, Device> KeyAndValueDevice = LDevice.stream()
                .collect(Collectors.toMap(Device::getId, Function.identity()));

        System.out.println("Map  with key and values");
        KeyAndValueDevice.forEach((K, D) -> System.out.println("The map key is: " + K + " and the value is: " + D.toString()));

    }
}
