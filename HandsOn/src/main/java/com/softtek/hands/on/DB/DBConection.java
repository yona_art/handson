package com.softtek.hands.on.DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.softtek.hands.on.Models.Device;

/**
 * DBConection
 */
public class DBConection {

    private static DBConection Db;
    public Connection conn;

    private DBConection() {
    }

    public static DBConection getDbConection() {
        if (Db == null) {
            return new DBConection();
        } else {
            return Db;
        }
    }

    public List<Device> getAllDevices() {
        try (Connection con =  DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#",
        "root", "1234")){
            ResultSet result = con.prepareStatement("SELECT * FROM Device").executeQuery();
            List<Device> LDevice = new ArrayList<Device>();
            while (result.next()) {
                LDevice.add(new Device(result.getInt("DeviceId"), result.getString("Name"),
                result.getString("Description"), result.getString("Comments"), result.getInt("ColorId"),
                        result.getInt("ManufacturerId")));
            }
            return LDevice;
        } catch (SQLException e) {
            System.out.println("The Query has an Errror");
            System.out.println("Error: " + e);
            return null;
        }
    }
}