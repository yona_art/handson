package com.softtek.hands.on.Models;

/**
 * Device
 */
public class Device {
    private int id;
    private String name;
    private String description;
    private String Comments;
    private int ColorId;
    private int ManufacturerId;

    public Device(int id, String name, String desc, String comm, int colorId, int manoID) {
        this.id = id;
        this.name = name;
        this.description = desc;
        this.Comments = comm;
        this.ColorId = colorId;
        this.ManufacturerId = manoID;
    }

    @Override
    public String toString() {
        return "{ id: " + id + " name: " + name + " Description: " + description + " Comments: " + Comments
                + " ColorId:" + ColorId + " ManufacturerId:" + ManufacturerId + " }";
    }

    public int getId() {
        return id;
    }

    public int getManufacturerId() {
        return ManufacturerId;
    }

    public void setManufacturerId(int manufacturerId) {
        this.ManufacturerId = manufacturerId;
    }

    public int getColorId() {
        return ColorId;
    }

    public void setColorId(int colorId) {
        this.ColorId = colorId;
    }

    public String getComments() {
        return Comments;
    }

    public void setComments(String comments) {
        this.Comments = comments;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

}